package tech.mastertech.itau.exemplo;

public enum Segmento {
	POBRITE("pobrité", 1),
	UNICLASS("uniclass", 2),
	PERSONNALITE("personnalité", 3);
	
	private String valor;
	private int codigo;

	private Segmento(String valor, int codigo){
		this.valor = valor;
		this.codigo = codigo;
	}
	
	public int getCodigo(){
		return codigo;
	}
	
	public String toString(){
		return valor;
	}
}