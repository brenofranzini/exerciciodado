package tech.mastertech.itau.dado;

public class Resultado {
	private int[] resultado;

	public Resultado(int[] resultado) {
		this.resultado = resultado;

	}

	public int somar() {
		int soma = 0;
		for (int valor : resultado)
			soma += valor;
		return soma;
	}

	public String toString() {
		String saida = new String();
		for (int valor : resultado)
			saida += valor + ", ";
		saida += somar();
		
		return saida;
	}

}
