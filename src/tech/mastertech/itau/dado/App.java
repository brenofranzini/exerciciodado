package tech.mastertech.itau.dado;

import tech.mastertech.itau.exemplo.Segmento;

public class App {

	public static void main(String[] args) {
		Dado dado = new Dado();
		Resultado[] resultado = Sorteador.sortear(dado, 3, 3);

		Saida.mostrar(resultado);

		System.out.println(Segmento.PERSONNALITE);
		System.out.println(Segmento.PERSONNALITE.getCodigo());
	}

}
