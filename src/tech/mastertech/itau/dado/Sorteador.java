package tech.mastertech.itau.dado;

public class Sorteador {

	private static int sortear(Dado dado) {
		return dado.jogar();
	}

	public static Resultado[] sortear(Dado dado, int jogadas, int grupos) {
		Resultado[] resultados = new Resultado[jogadas];
		for (int i = 0; i < grupos; i++)
		{
			int[] resultado = new int[jogadas];
			
			for (int j = 0; j < resultados.length; j++) {
				resultado[j] = sortear(dado);
			}
			resultados[i]  = new Resultado(resultado);
		}
		return resultados;
	}
}
