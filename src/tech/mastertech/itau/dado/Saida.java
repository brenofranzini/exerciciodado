package tech.mastertech.itau.dado;

public class Saida {

	public static void mostrar(Object[] sorteados) {
		for (Object valor : sorteados)
			System.out.println(valor);
	}
	
	public static void mostrar(Object resultado) {
		System.out.println(resultado);
	}
}
